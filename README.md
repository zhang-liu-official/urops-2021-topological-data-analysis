# UROPS 2021 Topological Data Analysis

## Project Title: Persistent Homology of Semantic Spaces 

 

## Objective: 

The aim of the project is to investigate the effectiveness of applying methods in the emerging field of topological data analysis to solve problems that involve natural language data. We will study the state-of-the-art method of persistent homology and several notable applications in computational analysis of natural language. In particular, we hope to investigate how effective the topological approach is compared to the traditional regression-based tools and why, which is still an open problem (Savle et al., 2019). 

 
## Potential list of deliverables: 

Literature review detailing the current state of research and the open problems 

A study report explaining the theoretical foundations of topological data analysis 

A study report explaining the other pre-requisite knowledge 

Code for relevant implementations or proposed approach/solution to the problem identified 

A final report and/or presentation detailing the topics learned and findings 
 

## Methods/Techniques: 

In this project, we study the mathematical foundations of the state-of-the-art methods in topological data analysis as well as the existing semantic space models. Further, we aim to investigate the effectiveness of these methods as well as to propose and possibly implement some improvements.  

 

## Estimated timeline: 

 

Week 1: Select topic  

Milestone: write up project proposal 

 

Week 2: Conduct literature Review 

Milestone: write up report summarizing previous research and an overview of the pre-requisites 

 

Week 3 to 4: Study the basics of Topological Data Analysis 

Milestone: write up report explaining the theories and/or techniques 

 

Week 5 to 6: Study relevant applications of topology in Mathematical Linguistics 

Milestone: write up report explaining the theories and/or techniques 

 

Recess Week 

(Finish the remaining pre-requisite knowledge, if any. Try to identify some problems/approaches for discussion in Week 7.) 

 

Week 7: Define a problem and propose some possible approach(es).  

Milestone: write up a tentative abstract 

 

Week 8 to 9: Implement preliminary experiments, with help from the code and tutorials available online.  

Milestone: potentially some code and/or solutions for subproblems 

 

Week 10: Reformulate and evaluate the proposed problem/approaches and the direction of the research.  

Milestone: write up a report for the findings/discussion 

 

Week 11 to 12: Continue with the experiment. / Study relevant theories (if we realize that more theories are needed to understand/solve the problem) 

Milestone: potentially some code and/or solutions for subproblems 

 

Week 13 and end of semester: Conclude the project and summarize the findings.  

Milestone: a final report and a presentation (including topics learned, experiments/implementations, findings, and discussions) 
 

Relevance: 
The field of topological Data Analysis has been growing rapidly in the recent two decades since its emergence. However, research in investigating its effectiveness when applied to natural language data presents several interesting open problems. The work by Savle et al. confirmed the ability of topological features to effectively capture certain structural properties of discourse text. However, they acknowledged in the conclusion that “why exactly topological methods work on entailment is an open problem.” Michel et al. concluded contradictory results that topology-based document representations perform worse than simple techniques like tf-idf. We hope to take this uncertainty as a departure point for our investigation into the relevant existing topological methods and models for natural language data and their effectiveness.  

 

## Notable existing research: 

Topological Data Analysis for Discourse Semantics? (in 2019 by researchers at UNC Charlotte) 

https://www.aclweb.org/anthology/W19-0605.pdf 

 

Does the Geometry of Word Embeddings Help Document Classification? A Case Study on Persistent Homology Based Representations (in 2017 by researchers at CMU) 

https://www.aclweb.org/anthology/W17-2628.pdf 

 

Semantic Spaces (in 2016 by researchers at Caltech) 

http://www.its.caltech.edu/~matilde/SemanticSpacesMCS.pdf 

http://www.its.caltech.edu/~matilde/LinguisticsToronto17.pdf 

 

Persistent Topology of Syntax (in 2017 by researchers at Caltech) 

http://www.its.caltech.edu/~matilde/LinguisticsToronto8.pdf 

https://link.springer.com/article/10.1007/s11786-017-0329-x 

 

## Other selected relevant works:  

Movie Genre Detection Using Topological Data Analysis 2018 

https://s3.amazonaws.com/cdn.ayasdi.com/wp-content/uploads/2018/10/19143136/Doshi-Zadrozny2018_Chapter_MovieGenreDetectionUsingTopolo-Final-Official-Springer-2.pdf 

Topology of Word Embeddings: Singularities Reflect Polysemy 

https://www.aclweb.org/anthology/2020.starsem-1.11.pdf 

Architecture and evolution of semantic networks in mathematics texts 

https://royalsocietypublishing.org/doi/full/10.1098/rspa.2019.0741#d29369511e1 

The Emergence of Higher-Order Structure in Scientific and Technological Knowledge Networks 

https://arxiv.org/pdf/2009.13620.pdf 

A Topological Representation of Information: A Heuristic Study 

https://scialert.net/fulltext/?doi=jas.2008.3743.3747 

Persistent Homology for Natural Data Analysis 

 

## Some resources for self-study: 

Topological data analysis  

CMU TopStat (http://www.stat.cmu.edu/topstat/research.html) 

Video tutorials: 

Introduction to Persistent Homology 


Stanford Seminar - Topological Data Analysis: How Ayasdi used TDA to Solve Complex Problems 


The mapper 

Gunnar Carlsson: "Topological Modeling of Complex Data" 

